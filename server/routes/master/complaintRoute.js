const express = require("express");

const {
  createComplaint,
  getAllComplaints,
  getSingleComplaint,
  updateComplaint,
  deleteComplaint,
} = require("../../controllers/master/complaintController");

const router = express.Router();

router.route("/complaint/new").post(createComplaint);

router.route("/complaint").get(getAllComplaints);

router
  .route("/complaint/:id")
  .get(getSingleComplaint)
  .put(updateComplaint)
  .delete(deleteComplaint);

module.exports = router;
