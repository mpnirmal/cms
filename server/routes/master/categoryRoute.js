const express = require("express");

const {
  deleteCategory,
  getAllCategory,
  createCategory,
  getSingleCategory,
  updateCategory,
  getGroupByCategory,
} = require("../../controllers/master/categoryController");

const router = express.Router();

router.route("/category/new").post(createCategory);

router.route("/category").get(getAllCategory);

router.route("/category_with_group/:id").get(getGroupByCategory);

router
  .route("/category/:id")
  .get(getSingleCategory)
  .put(updateCategory)
  .delete(deleteCategory);

module.exports = router;
