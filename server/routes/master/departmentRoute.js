const express = require("express");

const {
  createDepartment,
  getAllDepartment,
  getSingleDepartment,
  updateDepartment,
  deleteDepartment,
} = require("../../controllers/master/departmentController");

const router = express.Router();

router.route("/department/new").post(createDepartment);

router.route("/department").get(getAllDepartment);

router
  .route("/department/:id")
  .get(getSingleDepartment)
  .put(updateDepartment)
  .delete(deleteDepartment);

module.exports = router;
