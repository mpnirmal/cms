const express = require("express");

const {
  createFacility,
  getAllFacility,
  getSingleFacility,
  updateFacility,
  deleteFacility,
  getDepartmentByFacility,
} = require("../../controllers/master/facilityController");

const router = express.Router();

router.route("/facility/new").post(createFacility);

router.route("/facility").get(getAllFacility);

router.route("/facility_with_dept/:id").get(getDepartmentByFacility);

router
  .route("/facility/:id")
  .get(getSingleFacility)
  .put(updateFacility)
  .delete(deleteFacility);

module.exports = router;
