const express = require("express");

const {
  getSingleComplaint_Category,
  updateComplaint_Category,
  deleteComplaint_Category,
  getAllComplaint_Category,
  createComplaint_Category,
} = require("../../controllers/master/comp_cat_controllers");

const router = express.Router();

router.route("/comp-category/new").post(createComplaint_Category);

router.route("/comp-category").get(getAllComplaint_Category);

router
  .route("/comp-category/:id")
  .get(getSingleComplaint_Category)
  .put(updateComplaint_Category)
  .delete(deleteComplaint_Category);

module.exports = router;
