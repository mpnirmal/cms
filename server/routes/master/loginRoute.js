const express = require("express");

const {
  createNewLogin,
  getAllLoginLists,
  updateLoginLsit,
  deleteLoginList,
  getSingleLoginList,
} = require("../../controllers/master/loginController");

const router = express.Router();

router.route("/login/new").post(createNewLogin);

router.route("/login").get(getAllLoginLists);

router
  .route("/login/:id")
  .get(getSingleLoginList)
  .put(updateLoginLsit)
  .delete(deleteLoginList);

module.exports = router;
