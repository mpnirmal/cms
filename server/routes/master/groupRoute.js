const express = require("express");

const {
  createGroup,
  getAllGroup,
  getSingleGroup,
  updateGroup,
  deleteGroup,
} = require("../../controllers/master/groupController");

const router = express.Router();

router.route("/group/new").post(createGroup);

router.route("/group_list").get(getAllGroup);

router
  .route("/group/:id")
  .get(getSingleGroup)
  .put(updateGroup)
  .delete(deleteGroup);

module.exports = router;
