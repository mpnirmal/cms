const express = require("express");

const {
  createCallreason,
  getAllCallreasons,
  getSingleCallreason,
  updateCallreason,
  deleteCallreason,
} = require("../../controllers/master/callreasonController");

const router = express.Router();

router.route("/callreason/new").post(createCallreason);

router.route("/callreason").get(getAllCallreasons);

router
  .route("/callreason/:id")
  .get(getSingleCallreason)
  .put(updateCallreason)
  .delete(deleteCallreason);

module.exports = router;
