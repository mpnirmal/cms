const express = require("express");

const {
  createCasetype,
  getSingleCasetype,
  getAllCasetype,
  updateCasetype,
  deleteCasetype,
} = require("../../controllers/master/casetypeController");

const router = express.Router();

router.route("/casetype/new").post(createCasetype);

router.route("/casetype").get(getAllCasetype);

router
  .route("/casetype/:id")
  .get(getSingleCasetype)
  .put(updateCasetype)
  .delete(deleteCasetype);

module.exports = router;
