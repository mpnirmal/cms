const express = require("express");

const {
  createChannel,
  updateChannel,
  deleteChannel,
  getSingleChannel,
  getAllChannels,
} = require("../../controllers/master/channelController");

const router = express.Router();

router.route("/channel/new").post(createChannel);

router.route("/channel").get(getAllChannels);

router
  .route("/channel/:id")
  .get(getSingleChannel)
  .put(updateChannel)
  .delete(deleteChannel);

module.exports = router;
