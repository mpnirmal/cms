const express = require("express");

const {
  createRoleList,
  getAllRoleLists,
  getSingleRole,
  updateRole,
  deleteRole,
} = require("../../controllers/master/rolelistController");

const router = express.Router();

router.route("/role/new").post(createRoleList);

router.route("/role").get(getAllRoleLists);

router.route("/role/:id").get(getSingleRole).put(updateRole).delete(deleteRole);

module.exports = router;
