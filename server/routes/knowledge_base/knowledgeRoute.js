const express = require("express");

const {
  createKnowledgeBase,
  getAllKnowledgeBase,
  getSingleKnowledgeBase,
  updateKnowledgeBase,
  deleteKnowledgeBase,
  getFile,
} = require("../../controllers/knowledge_base/knowledgeController");
const singleUpload = require("../../middleware/multer");

const router = express.Router();

router.route("/knowledge_base/new").post(singleUpload, createKnowledgeBase);

router.route("/knowledge_base").get(getAllKnowledgeBase);

router
  .route("/knowledge_base/:id")
  .get(getSingleKnowledgeBase)
  .put(singleUpload, updateKnowledgeBase)
  .delete(deleteKnowledgeBase);

router.route("/knowledge_base/file/:filename").get(getFile);

module.exports = router;
