const multer = require("multer");
const ErrorHandler = require("../utils/errorHandler");

const storage = multer.memoryStorage();

const singleUpload = multer({
  storage,
  fileFilter: (req, file, cb) => {
    const allowedTypes = [
      "image/jpeg",
      "image/png",
      "image/jpg",
      "application/pdf",
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      "application/msword",
      "text/plain",
      "application/vnd.ms-excel",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "text/csv",
    ];

    if (allowedTypes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      const reason = `Invalid file type: ${
        file.mimetype
      }. Only ${allowedTypes.join(", ")} are allowed.`;

      cb(new ErrorHandler(reason, 400));
    }
  },
  limits: { fileSize: 5 * 1024 * 1024 },
}).single("file");

module.exports = singleUpload;
