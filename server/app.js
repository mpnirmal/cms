const express = require("express");
const app = express();
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require("cors");
const { join } = require("path");
const { static, json } = require("express");

const ErrorHandler = require("./utils/errorHandler");
const errorMiddleware = require("./middleware/error");

// import routes
const category = require("./routes/master/categoryRoute");
const group = require("./routes/master/groupRoute");
const facility = require("./routes/master/facilityRoute");
const casetype = require("./routes/master/casetypeRoute");
const channel = require("./routes/master/channelRoute");
const department = require("./routes/master/departmentRoute");
const callreason = require("./routes/master/callreasonRoute");
const complaintCategory = require("./routes/master/comp_cat_Route");
const complaint = require("./routes/master/complaintRoute");
const role = require("./routes/master/RolelistRoute");
const login = require("./routes/master/loginRoute");

const knowledge = require("./routes/knowledge_base/knowledgeRoute");

app.use(
  cors({
    origin: `*`,
    methods: ["GET", "POST", "PUT", "DELETE", "PATCH"],
  })
);

app.use(json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true, limit: "5mb" }));

app.get("/", (req, res) => res.send(`cms server is working fine`));

app.use("/uploads", static(join(`${__dirname}/uploads`, "uploads")));

// Apis for masters
app.use("/api/v1", category);
app.use("/api/v1", group);
app.use("/api/v1", facility);
app.use("/api/v1", casetype);
app.use("/api/v1", channel);
app.use("/api/v1", department);
app.use("/api/v1", callreason);
app.use("/api/v1", complaintCategory);
app.use("/api/v1", complaint);
app.use("/api/v1", role);
app.use("/api/v1", login);

// Apis for knowledge base
app.use("/api/v1", knowledge);

// Page Not Found
app.all("*", (req, res, next) => {
  next(new ErrorHandler(`${req.originalUrl} Resource Not Found`, 404));
});

// Error handling middleware
app.use(errorMiddleware);

module.exports = app;
