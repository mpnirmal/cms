class ApiFeatures {
  constructor(permission) {
    this.permission = permission;
    this.check = [];
  }

  checkPermission() {
    for (let key in this.permission) {
      if (this.permission.hasOwnProperty(key)) {
        if (typeof this.permission[key] === "object") {
          this.nestedObject(this.permission[key], key);
        } else if (this.permission[key] === 1) {
          check.push(`${this.permission}_${key}`);
        }
      }
    }

    return this.check;
  }

  nestedObject(perm, objName) {
    for (let key in perm) {
      if (perm.hasOwnProperty(key)) {
        if (typeof perm[key] === "object") {
          checkPermission(perm[key], key);
        } else if (perm[key] === 1) {
          this.check.push(`${objName}_${key}`);
        }
      }
    }

    return this.check;
  }
}

module.exports = ApiFeatures;
