const mongoose = require("mongoose");

const complaintCategorySchema = new mongoose.Schema(
  {
    complaint_category: {
      type: String,
      required: [true, "Please Enter Channel Name"],
      trim: true,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Complaint_Category", complaintCategorySchema);
