const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema(
  {
    category_name: {
      type: String,
      required: [true, "Please Enter Category Name"],
      trim: true,
      // validate: {
      //   validator: function (val) {
      //     return /^[a-zA-Z]+$/.test(val);
      //   },
      //   message: (val) =>
      //     `${val.value} is not a valid, category name must be contain only alphapet characters.`,
      // },
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },

    group_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Group",
      default: null,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Category", categorySchema);
