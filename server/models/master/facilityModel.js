const mongoose = require("mongoose");

const faciltySchema = new mongoose.Schema(
  {
    facility_name: {
      type: String,
      required: [true, "Please Enter Facility Name"],
      trim: true,
    },

    is_deleted: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    is_active: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Facility", faciltySchema);
