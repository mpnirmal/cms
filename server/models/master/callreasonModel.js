const mongoose = require("mongoose");

const callreasonSchema = new mongoose.Schema(
  {
    callreason_name: {
      type: String,
      required: [true, "Please Enter Call Reason Name"],
      trim: true,
    },

    is_deleted: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Callreason", callreasonSchema);
