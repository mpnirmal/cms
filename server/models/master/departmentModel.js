const mongoose = require("mongoose");

const departmentSchema = new mongoose.Schema(
  {
    department_name: {
      type: String,
      required: [true, "Please Enter Department Name"],
      trim: true,
    },

    facility_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Facility",
      required: [true, "Please Select Facility Name"],
    },

    dept_survey: {
      type: String,
      trim: true,
      default: null,
    },

    dept_email: {
      type: String,
      default: null,
    },

    email_cc: {
      type: String,
      default: null,
    },

    label: {
      type: String,
      default: null,
    },

    is_deleted: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    is_active: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Department", departmentSchema);
