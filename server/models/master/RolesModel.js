const mongoose = require("mongoose");
// const { v4: uuidv4 } = require("uuid");

const rolesSchema = new mongoose.Schema(
  {
    role_name: {
      type: String,
      required: [true, "Please Enter Role Name"],
      trim: true,
    },

    description: {
      type: String,
      trim: true,
    },

    organization_id: {
      type: mongoose.Schema.ObjectId,
      // ref: "Organization",
      // required: [true, "Please Select Organization Name"],
      default: null,
    },

    feature_id: {
      type: mongoose.Schema.ObjectId,
      // ref: "Feature",
      // required: [true, "Please Select Feature Name"],
      default: null,
    },

    default_role: {
      type: String,
      enum: ["Admin", "Agent", "Department User", "Supervisor", "Test"],
      default: "Agent",
    },

    permission: {
      Dashboard: {
        dashboard: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
      },

      Customers: {
        addcustomer: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewcustomer: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        editcustomer: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        deletecustomer: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
      },

      Tickets: {
        viewparentticket: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewticketassignability: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewautomatedticket: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewabilitytocloseticket: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewticketreopen: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewticket: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        addticket: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
      },

      Reports: {
        viewallreport: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        exportreport: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
      },

      Kbase: {
        viewkbase: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        addkbase: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        editkbase: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        deletekbase: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
      },

      Masters: {
        viewmasters: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewmasterskbase: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewmasterschannel: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewmastersusers: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewmasterscomplaint: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
        viewmastersautomation: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
      },

      Screenpopup: {
        viewscreenpopup: {
          type: Number,
          enum: [0, 1],
          default: 0,
        },
      },
    },

    permission_allowed: { type: [String] },

    role_scope: {
      type: String,
      enum: ["GA", "LA", "AG", "SA", "DA"],
      default: "AG",
    },

    is_active: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Role", rolesSchema);
