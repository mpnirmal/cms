const mongoose = require("mongoose");

const complaintSchema = new mongoose.Schema(
  {
    complaint: {
      type: String,
      required: [true, "Please Enter Complaint Name"],
      trim: true,
    },

    complaint_catg_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Complaint_Category",
      required: [true, "Please Select Complaint Category Name"],
    },

    dept_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Department",
      required: [true, "Please Select Department Name"],
    },

    // facility_id: {
    //   type: mongoose.Schema.ObjectId,
    //   ref: "Facility",
    //   required: [true, "Please Select Facility Name"],
    // },

    complaint_code: {
      type: Number,
      default: null,
    },

    complaint_msg: {
      type: String,
      trim: true,
      default: null,
    },

    priority: {
      type: String,
      trim: true,
      default: null,
    },

    is_deleted: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
        delete ret.createdAt;
      },
    },
  }
);

module.exports = mongoose.model("Complaint", complaintSchema);
