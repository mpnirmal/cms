const mongoose = require("mongoose");
const validator = require("validator");

const loginSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please Enter Your Name"],
      maxLength: [30, "Name cannot exceed 30 characters"],
      minLength: [3, "Name should have more than 3 characters"],
    },

    user_email: {
      type: String,
      required: [true, "Please Enter Your Email"],
      unique: true,
      validate: [validator.isEmail, "Please Enter Valid Email"],
    },

    signup_username: {
      type: String,
      required: [true, "Please Enter Your Email"],
      unique: true,
      validate: [validator.isEmail, "Please Enter Valid Email"],
    },

    signup_password: {
      type: String,
      required: [true, "Please Enter Your Password"],
      minLength: [8, "Password should be greater than 8 characters"],
      select: false,
    },

    organization_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Role",
      default: null,
    },

    role_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Role",
      required: true,
    },

    facility_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Facility",
      required: true,
    },

    department_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Department",
      required: true,
    },

    channel_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Channel",
      required: true,
    },

    division_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Channel",
      default: null,
    },

    scope: {
      type: String,
      enum: ["GA", "LA", "AG", "SA", "DA"],
      required: true,
    },

    usertype: {
      type: String,
      default: null,
    },

    is_deleted: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    is_active: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Login", loginSchema);
