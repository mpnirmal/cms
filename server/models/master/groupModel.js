const mongoose = require("mongoose");

const groupSchema = new mongoose.Schema(
  {
    group_name: {
      type: String,
      required: [true, "Please Enter Group Name"],
      trim: true,
    },

    category_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Categroy",
      required: [true, "Please Select Category Name"],
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Group", groupSchema);
