const mongoose = require("mongoose");

const casetypeSchema = new mongoose.Schema(
  {
    case_type: {
      type: String,
      required: [true, "Please Enter Case Type Name"],
      trim: true,
    },

    case_label: {
      type: String,
      required: [true, "Please Enter Case label Name"],
      trim: true,
    },

    case_flag: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    is_active: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Casetype", casetypeSchema);
