const mongoose = require("mongoose");

const channelSchema = new mongoose.Schema(
  {
    channel_name: {
      type: String,
      required: [true, "Please Enter Channel Name"],
      trim: true,
    },

    is_active: {
      type: Number,
      enum: [0, 1],
      default: 0,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Channel", channelSchema);
