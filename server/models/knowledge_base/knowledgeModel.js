const mongoose = require("mongoose");

const knowledgeSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Please Enter Title Name"],
      trim: true,
    },

    description: {
      type: String,
      required: [true, "Please Enter Description"],
      trim: true,
    },

    category_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Categroy",
      required: [true, "Please Select Category"],
    },

    group_id: {
      type: mongoose.Schema.ObjectId,
      ref: "Group",
    },

    filename: { type: String },

    category_name: {
      type: String,
      required: [true, "Please Select Category Name"],
      trim: true,
    },

    group_name: {
      type: String,
      required: [true, "Please Select Group Name"],
      trim: true,
    },

    files: {
      type: String,
      required: true,
    },

    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        delete ret.__v;
      },
    },
  }
);

module.exports = mongoose.model("Knowledge", knowledgeSchema);
