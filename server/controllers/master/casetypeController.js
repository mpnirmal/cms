const createAsyncError = require("../../middleware/createAsyncError");
const Casetype = require("../../models/master/casetypeModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Casetype
exports.createCasetype = createAsyncError(async (req, res, next) => {
  const casetype = await Casetype.create(req.body);

  res.status(201).json({
    success: true,
    message: "Casetype added successfully",
    casetype,
  });
});

// Get All Casetypes
exports.getAllCasetype = createAsyncError(async (req, res, next) => {
  const casetype = await Casetype.find();

  res.status(200).json({
    success: true,
    message: "Casetype List",
    data: {
      case_type_list: casetype,
    },
  });
});

// Get CaseType Detail
exports.getSingleCasetype = createAsyncError(async (req, res, next) => {
  let casetype = await Casetype.findById(req.params.id);

  if (!casetype) {
    return next(new ErrorHandler("Casetype not found", 404));
  }

  res.status(200).json({ success: true, casetype });
});

// Update Casetype
exports.updateCasetype = createAsyncError(async (req, res, next) => {
  let casetype = await Casetype.findById(req.params.id);

  if (!casetype) {
    return next(new ErrorHandler("Casetype not found", 404));
  }

  casetype = await Casetype.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Casetype updated successfully",
    data: casetype,
  });
});

// Delete Casetype
exports.deleteCasetype = createAsyncError(async (req, res, next) => {
  const casetype = await Casetype.findById(req.params.id);

  if (!casetype) {
    return next(new ErrorHandler("Casetype not found", 404));
  }

  await casetype.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Casetype deleted successfully",
  });
});
