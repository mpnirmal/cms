const createAsyncError = require("../../middleware/createAsyncError");
const Category = require("../../models/master/categoryModel");
const Group = require("../../models/master/groupModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Group
exports.createGroup = createAsyncError(async (req, res, next) => {
  let category = await Category.findById(req.body.category_id);

  if (!category) {
    return next(new ErrorHandler("Category not found", 404));
  }

  const group = await Group.create(req.body);

  res.status(201).json({
    success: true,
    message: "Group added successfully",
    group,
  });
});

// Get All Group
exports.getAllGroup = createAsyncError(async (req, res, next) => {
  const groups = await Group.find();

  res.status(200).json({
    success: true,
    message: "Group List",
    data: {
      group_list: groups,
    },
  });
});

// Get Group Detail
exports.getSingleGroup = createAsyncError(async (req, res, next) => {
  const group = await Group.findById(req.params.id);

  if (!group) {
    return next(new ErrorHandler("Group not found", 404));
  }

  res.status(200).json({ success: true, group });
});

// Update Group
exports.updateGroup = createAsyncError(async (req, res, next) => {
  let group = await Group.findById(req.params.id);

  if (!group) {
    return next(new ErrorHandler("Group not found", 404));
  }

  if (req.body.category_id) {
    const category = await Category.findById(req.body.category_id);

    if (!category) {
      return next(new ErrorHandler("Category not found", 404));
    }
  }

  group = await Group.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Group updated successfully",
    data: group,
  });
});

// Delete Group
exports.deleteGroup = createAsyncError(async (req, res, next) => {
  const group = await Group.findById(req.params.id);

  if (!group) {
    return next(new ErrorHandler("Group not found", 404));
  }

  await Group.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Group deleted successfully",
  });
});
