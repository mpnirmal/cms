const createAsyncError = require("../../middleware/createAsyncError");
const Complaint_Category = require("../../models/master/comp_cat_Model");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Complaint_Category
exports.createComplaint_Category = createAsyncError(async (req, res, next) => {
  const complaintCategory = await Complaint_Category.create(req.body);

  res.status(201).json({
    success: true,
    message: "Complaint Category added successfully",
    complaintCategory,
  });
});

// Get All Complaint_Category
exports.getAllComplaint_Category = createAsyncError(async (req, res, next) => {
  const complaintCategory = await Complaint_Category.find();

  res.status(200).json({
    success: true,
    message: "Complaint Category List",
    data: {
      complaint_category_list: complaintCategory,
    },
  });
});

// Get Complaint_Category Detail
exports.getSingleComplaint_Category = createAsyncError(
  async (req, res, next) => {
    let complaintCategory = await Complaint_Category.findById(req.params.id);

    if (!complaintCategory) {
      return next(new ErrorHandler("Complaint Category not found", 404));
    }

    res.status(200).json({ success: true, complaintCategory });
  }
);

// Update Complaint_Category
exports.updateComplaint_Category = createAsyncError(async (req, res, next) => {
  let complaintCategory = await Complaint_Category.findById(req.params.id);

  if (!complaintCategory) {
    return next(new ErrorHandler("Complaint Category not found", 404));
  }

  complaintCategory = await Complaint_Category.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true,
      runValidators: true,
      useFindAndModify: false,
    }
  );

  res.status(200).json({
    success: true,
    message: "Complaint Category updated successfully",
    data: complaintCategory,
  });
});

// Delete Complaint_Category
exports.deleteComplaint_Category = createAsyncError(async (req, res, next) => {
  const complaintCategory = await Complaint_Category.findById(req.params.id);

  if (!complaintCategory) {
    return next(new ErrorHandler("Complaint Category not found", 404));
  }

  await complaintCategory.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Complaint Category deleted successfully",
  });
});
