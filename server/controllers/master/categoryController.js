const createAsyncError = require("../../middleware/createAsyncError");
const Category = require("../../models/master/categoryModel");
const Group = require("../../models/master/groupModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Category
exports.createCategory = createAsyncError(async (req, res, next) => {
  const category = await Category.create(req.body);

  res.status(201).json({
    success: true,
    message: "Category added successfully",
    category,
  });
});

// Get All Category
exports.getAllCategory = createAsyncError(async (req, res, next) => {
  const category = await Category.find();

  res.status(200).json({
    success: true,
    message: "Category List",
    data: {
      category_list: category,
    },
  });
});

// Get Category Detail
exports.getSingleCategory = createAsyncError(async (req, res, next) => {
  let category = await Category.findById(req.params.id);

  if (!category) {
    return next(new ErrorHandler("Category not found", 404));
  }

  res.status(200).json({ success: true, category });
});

exports.getGroupByCategory = createAsyncError(async (req, res, next) => {
  const category = await Category.findById(req.params.id);

  if (!category) {
    return next(new ErrorHandler("Category not found", 404));
  }

  const groupByCategory = await Group.find({ category_id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Group List Based on Category",
    data: { group_list: groupByCategory },
  });
});

// Update Category
exports.updateCategory = createAsyncError(async (req, res, next) => {
  let category = await Category.findById(req.params.id);

  if (!category) {
    return next(new ErrorHandler("Category not found", 404));
  }

  category = await Category.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Category updated successfully",
    data: category,
  });
});

// Delete Category
exports.deleteCategory = createAsyncError(async (req, res, next) => {
  const category = await Category.findById(req.params.id);

  if (!category) {
    return next(new ErrorHandler("Category not found", 404));
  }

  await Group.deleteMany({ category_id: req.params.id });
  await Category.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Category deleted successfully",
  });
});
