const createAsyncError = require("../../middleware/createAsyncError");
const CallReason = require("../../models/master/callreasonModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Callreason
exports.createCallreason = createAsyncError(async (req, res, next) => {
  const callreason = await CallReason.create(req.body);

  res.status(201).json({
    success: true,
    message: "Callreason added successfully",
    callreason,
  });
});

// Get All Callreason
exports.getAllCallreasons = createAsyncError(async (req, res, next) => {
  const callreasons = await CallReason.find();

  res.status(200).json({
    success: true,
    message: "Callreason List",
    data: {
      callreason_list: callreasons,
    },
  });
});

// Get Callreason Detail
exports.getSingleCallreason = createAsyncError(async (req, res, next) => {
  let callreason = await CallReason.findById(req.params.id);

  if (!callreason) {
    return next(new ErrorHandler("Callreason not found", 404));
  }

  res.status(200).json({ success: true, callreason });
});

// Update Callreason
exports.updateCallreason = createAsyncError(async (req, res, next) => {
  let callreason = await CallReason.findById(req.params.id);

  if (!callreason) {
    return next(new ErrorHandler("Callreason not found", 404));
  }

  callreason = await CallReason.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Callreason updated successfully",
    data: callreason,
  });
});

// Delete Callreason
exports.deleteCallreason = createAsyncError(async (req, res, next) => {
  const callreason = await CallReason.findById(req.params.id);

  if (!callreason) {
    return next(new ErrorHandler("Callreason not found", 404));
  }

  await callreason.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Callreason deleted successfully",
  });
});
