const createAsyncError = require("../../middleware/createAsyncError");
const Department = require("../../models/master/departmentModel");
const Facility = require("../../models/master/facilityModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Department
exports.createDepartment = createAsyncError(async (req, res, next) => {
  let facility = await Facility.findById(req.body.facility_id);

  if (!facility) {
    return next(new ErrorHandler("Facility not found", 404));
  }

  const department = await Department.create(req.body);

  res.status(201).json({
    success: true,
    message: "Department added successfully",
    department,
  });
});

// Get All Department
exports.getAllDepartment = createAsyncError(async (req, res, next) => {
  const departments = await Department.find();

  res.status(200).json({
    success: true,
    message: "Department List",
    data: {
      department_list: departments,
    },
  });
});

// Get Department Detail
exports.getSingleDepartment = createAsyncError(async (req, res, next) => {
  const department = await Department.findById(req.params.id);

  if (!department) {
    return next(new ErrorHandler("Department not found", 404));
  }

  res.status(200).json({ success: true, department });
});

// Update Department
exports.updateDepartment = createAsyncError(async (req, res, next) => {
  let department = await Department.findById(req.params.id);

  if (!department) {
    return next(new ErrorHandler("Department not found", 404));
  }

  if (req.body.facility_id) {
    const facility = await Facility.findById(req.body.facility_id);

    if (!facility) {
      return next(new ErrorHandler("Facility not found", 404));
    }
  }

  department = await Department.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Department updated successfully",
    data: department,
  });
});

// Delete Department
exports.deleteDepartment = createAsyncError(async (req, res, next) => {
  const department = await Department.findById(req.params.id);

  if (!department) {
    return next(new ErrorHandler("Department not found", 404));
  }

  await department.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Department deleted successfully",
  });
});
