const createAsyncError = require("../../middleware/createAsyncError");
const ErrorHandler = require("../../utils/errorHandler");
const Login = require("../../models/master/loginModel");
const Role = require("../../models/master/RolesModel");
const Facility = require("../../models/master/facilityModel");
const Department = require("../../models/master/departmentModel");
const Channel = require("../../models/master/channelModel");

// Create New Login
exports.createNewLogin = createAsyncError(async (req, res, next) => {
  // const organization = await Organization.findById(
  //   req.body.organization_id
  // );

  // if (!organization) {
  //   return next(new ErrorHandler("Organization not found", 404));
  // }

  const role = await Role.findById(req.body.role_id);

  if (!role) {
    return next(new ErrorHandler("Role not found", 404));
  }

  const facility = await Facility.findById(req.body.facility_id);

  if (!facility) {
    return next(new ErrorHandler("Facility not found", 404));
  }

  const department = await Department.findById(req.body.department_id);

  if (!department) {
    return next(new ErrorHandler("Department not found", 404));
  }

  const channel = await Channel.findById(req.body.channel_id);

  if (!channel) {
    return next(new ErrorHandler("Channel not found", 404));
  }

  // const division = await Division.findById(req.body.division_id);

  // if (!division) {
  //   return next(new ErrorHandler("Division not found", 404));
  // }

  if (req.body.user_email === req.body.signup_username) {
    return next(
      new ErrorHandler("User_email & Signup_username should be different", 400)
    );
  }

  const login = await Login.create(req.body);

  res.status(201).json({
    success: true,
    message: "New Login added successfully",
    login,
  });
});

// Get All Login Lists
exports.getAllLoginLists = createAsyncError(async (req, res, next) => {
  const loginLists = await Login.find();

  res.status(200).json({
    success: true,
    message: "Login Master List",
    data: {
      organization_name: "Sample Name",
      login_list: loginLists,
    },
  });
});

// Get Login Detail
exports.getSingleLoginList = createAsyncError(async (req, res, next) => {
  const loginList = await Login.findById(req.params.id);

  if (!loginList) {
    return next(new ErrorHandler("Login List not found", 404));
  }

  res.status(200).json({ success: true, loginList });
});

// Update Login List
exports.updateLoginLsit = createAsyncError(async (req, res, next) => {
  let loginList = await Login.findById(req.params.id);

  if (!loginList) {
    return next(new ErrorHandler("Login List not found", 404));
  }

  // const organization = await Organization.findById(
  //   req.body.organization_id
  // );

  // if (!organization) {
  //   return next(new ErrorHandler("Organization not found", 404));
  // }

  const role = await Role.findById(req.body.role_id);

  if (!role) {
    return next(new ErrorHandler("Role not found", 404));
  }

  const facility = await Facility.findById(req.body.facility_id);

  if (!facility) {
    return next(new ErrorHandler("Facility not found", 404));
  }

  const department = await Department.findById(req.body.department_id);

  if (!department) {
    return next(new ErrorHandler("Department not found", 404));
  }

  const channel = await Channel.findById(req.body.channel_id);

  if (!channel) {
    return next(new ErrorHandler("Channel not found", 404));
  }

  // const division = await Division.findById(req.body.division_id);

  // if (!division) {
  //   return next(new ErrorHandler("Division not found", 404));
  // }

  if (req.body?.user_email === req.body?.signup_username) {
    return next(
      new ErrorHandler("User_email & Signup_username should be different", 400)
    );
  }

  loginList = await Login.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Login List updated successfully",
    data: loginList,
  });
});

// Delete Login List
exports.deleteLoginList = createAsyncError(async (req, res, next) => {
  const loginList = await Login.findById(req.params.id);

  if (!loginList) {
    return next(new ErrorHandler("Login List not found", 404));
  }

  await loginList.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Login List deleted successfully",
  });
});
