const createAsyncError = require("../../middleware/createAsyncError");
const ErrorHandler = require("../../utils/errorHandler");
const Role = require("../../models/master/RolesModel");
const ApiFeatures = require("../../utils/ApiFeatures");

// Create New RoleList
exports.createRoleList = createAsyncError(async (req, res, next) => {
  // const organization = await Organization.findById(
  //   req.body.organization_id
  // );

  // if (!organization) {
  //   return next(new ErrorHandler("Organization not found", 404));
  // }

  // const features = await Feature.findById(req.body.feature_id);

  // if (!features) {
  //   return next(new ErrorHandler("Feature not found", 404));
  // }

  // const check = [];
  // const permission = function (perm, objName) {
  //   for (let key in perm) {
  //     if (perm.hasOwnProperty(key)) {
  //       if (typeof perm[key] === "object") {
  //         permission(perm[key], key);
  //       } else if (perm[key] === 1) {
  //         check.push(`${objName}_${key}`);
  //       }
  //     }
  //   }
  // };

  // permission(req.body.permission);

  const perm_Allow = new ApiFeatures(req.body.permission).checkPermission();

  const role = await Role.create({
    ...req.body,
    permission_allowed: perm_Allow,
  });

  res.status(201).json({
    success: true,
    message: "Role added successfully",
    role,
  });
});

// Get All Role Lists
exports.getAllRoleLists = createAsyncError(async (req, res, next) => {
  const roles = await Role.find();

  res.status(200).json({
    success: true,
    message: "Role List",
    data: {
      feature: "",
      actions: "",
      roles: roles,
    },
  });
});

// Get Role Detail
exports.getSingleRole = createAsyncError(async (req, res, next) => {
  const role = await Role.findById(req.params.id);

  if (!role) {
    return next(new ErrorHandler("Role not found", 404));
  }

  res.status(200).json({ success: true, role });
});

// Update Role
exports.updateRole = createAsyncError(async (req, res, next) => {
  // const organization = await Organization.findById(
  //   req.body.organization_id
  // );

  // if (!organization) {
  //   return next(new ErrorHandler("Organization not found", 404));
  // }

  // const features = await Feature.findById(req.body.feature_id);

  // if (!features) {
  //   return next(new ErrorHandler("Feature not found", 404));
  // }

  let role = await Role.findById(req.params.id);

  if (!role) {
    return next(new ErrorHandler("Role not found", 404));
  }

  const perm_Allow = new ApiFeatures(req.body.permission).checkPermission();

  role = await Role.findByIdAndUpdate(
    req.params.id,
    { ...req.body, permission_allowed: perm_Allow },
    {
      new: true,
      runValidators: true,
      useFindAndModify: false,
    }
  );

  res.status(200).json({
    success: true,
    message: "Role updated successfully",
    data: role,
  });
});

// Delete Role
exports.deleteRole = createAsyncError(async (req, res, next) => {
  const role = await Role.findById(req.params.id);

  if (!role) {
    return next(new ErrorHandler("Role not found", 404));
  }

  await role.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Role deleted successfully",
  });
});
