const createAsyncError = require("../../middleware/createAsyncError");
const Channel = require("../../models/master/channelModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Channel
exports.createChannel = createAsyncError(async (req, res, next) => {
  const channel = await Channel.create(req.body);

  res.status(201).json({
    success: true,
    message: "Channel added successfully",
    channel,
  });
});

// Get All Channel
exports.getAllChannels = createAsyncError(async (req, res, next) => {
  const channels = await Channel.find();

  res.status(200).json({
    success: true,
    message: "Channel List",
    data: {
      channel_cbi: channels,
    },
  });
});

// Get Channel Detail
exports.getSingleChannel = createAsyncError(async (req, res, next) => {
  let channel = await Channel.findById(req.params.id);

  if (!channel) {
    return next(new ErrorHandler("Channel not found", 404));
  }

  res.status(200).json({ success: true, channel });
});

// Update Channel
exports.updateChannel = createAsyncError(async (req, res, next) => {
  let channel = await Channel.findById(req.params.id);

  if (!channel) {
    return next(new ErrorHandler("Channel not found", 404));
  }

  channel = await Channel.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Channel updated successfully",
    data: channel,
  });
});

// Delete Channel
exports.deleteChannel = createAsyncError(async (req, res, next) => {
  const channel = await Channel.findById(req.params.id);

  if (!channel) {
    return next(new ErrorHandler("Channel not found", 404));
  }

  await channel.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Channel deleted successfully",
  });
});
