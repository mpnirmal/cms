const createAsyncError = require("../../middleware/createAsyncError");
const Complaint_Category = require("../../models/master/comp_cat_Model");
const Department = require("../../models/master/departmentModel");
const Facility = require("../../models/master/facilityModel");
const Complaint = require("../../models/master/complaintModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Complaint
exports.createComplaint = createAsyncError(async (req, res, next) => {
  const complaintCategory = await Complaint_Category.findById(
    req.body.complaint_catg_id
  );

  if (!complaintCategory) {
    return next(new ErrorHandler("Complaint Category not found", 404));
  }

  const department = await Department.findById(req.body.dept_id);

  if (!department) {
    return next(new ErrorHandler("Department not found", 404));
  }

  //   const facility = await Facility.findById(req.body.facility_id);
  //   if (!facility) {
  //     return next(new ErrorHandler("Facility not found", 404));
  //   }

  const complaint = await Complaint.create(req.body);

  res.status(201).json({
    success: true,
    message: "Complaint added successfully",
    complaint,
  });
});

// Get All Complaints
exports.getAllComplaints = createAsyncError(async (req, res, next) => {
  const complaints = await Complaint.find()
    .populate("complaint_catg_id", ["complaint_category", "-_id"])
    .populate({
      path: "dept_id",
      model: "Department",
      select: { department_name: 1, _id: 0 },
      populate: {
        path: "facility_id",
        model: "Facility",
        select: { facility_name: 1, _id: 0 },
      },
    });

  res.status(200).json({
    success: true,
    message: "Complaint List",
    data: {
      complaint_list: complaints,
    },
  });
});

// Get Complaint Detail
exports.getSingleComplaint = createAsyncError(async (req, res, next) => {
  const complaint = await Complaint.findById(req.params.id)
    .populate("complaint_catg_id", ["complaint_category", "-_id"])
    .populate({
      path: "dept_id",
      model: "Department",
      select: { department_name: 1, _id: 0 },
      populate: {
        path: "facility_id",
        model: "Facility",
        select: { facility_name: 1, _id: 0 },
      },
    });

  if (!complaint) {
    return next(new ErrorHandler("Complaint not found", 404));
  }

  res.status(200).json({ success: true, complaint });
});

// Update Complaint
exports.updateComplaint = createAsyncError(async (req, res, next) => {
  let complaint = await Complaint.findById(req.params.id);

  if (!complaint) {
    return next(new ErrorHandler("Complaint not found", 404));
  }

  if (req.body.complaint_catg_id) {
    const complaintCategory = await Complaint_Category.findById(
      req.body.complaint_catg_id
    );

    if (!complaintCategory) {
      return next(new ErrorHandler("Complaint Category not found", 404));
    }
  }

  if (req.body.dept_id) {
    const department = await Department.findById(req.body.dept_id);

    if (!department) {
      return next(new ErrorHandler("Department not found", 404));
    }
  }

  complaint = await Complaint.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Complaint updated successfully",
    data: complaint,
  });
});

// Delete Complaint
exports.deleteComplaint = createAsyncError(async (req, res, next) => {
  const complaint = await Complaint.findById(req.params.id);

  if (!complaint) {
    return next(new ErrorHandler("Complaint not found", 404));
  }

  await complaint.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Complaint deleted successfully",
  });
});
