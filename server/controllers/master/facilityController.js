const createAsyncError = require("../../middleware/createAsyncError");
const Department = require("../../models/master/departmentModel");
const Facility = require("../../models/master/facilityModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Facility
exports.createFacility = createAsyncError(async (req, res, next) => {
  const facility = await Facility.create(req.body);

  res.status(201).json({
    success: true,
    message: "Facility added successfully",
    facility,
  });
});

// Get All Facilities
exports.getAllFacility = createAsyncError(async (req, res, next) => {
  const Facilities = await Facility.find();

  res.status(200).json({
    success: true,
    message: "Facility List",
    data: {
      facility_list: Facilities,
    },
  });
});

// Get Facility Detail
exports.getSingleFacility = createAsyncError(async (req, res, next) => {
  let facility = await Facility.findById(req.params.id);

  if (!facility) {
    return next(new ErrorHandler("Facility not found", 404));
  }

  res.status(200).json({ success: true, facility });
});

exports.getDepartmentByFacility = createAsyncError(async (req, res, next) => {
  const facility = await Facility.findById(req.params.id);

  if (!facility) {
    return next(new ErrorHandler("Facility not found", 404));
  }

  const deptByFacility = await Department.find({ facility_id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Department List Based on Facility",
    data: { departmentlist: deptByFacility },
  });
});

// Update Facility
exports.updateFacility = createAsyncError(async (req, res, next) => {
  let facility = await Facility.findById(req.params.id);

  if (!facility) {
    return next(new ErrorHandler("Facility not found", 404));
  }

  facility = await Facility.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Facility updated successfully",
    data: facility,
  });
});

// Delete Facility
exports.deleteFacility = createAsyncError(async (req, res, next) => {
  const facility = await Facility.findById(req.params.id);

  if (!facility) {
    return next(new ErrorHandler("Facility not found", 404));
  }

  await Department.deleteMany({ facility_id: req.params.id });
  await facility.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Facility deleted successfully",
  });
});
