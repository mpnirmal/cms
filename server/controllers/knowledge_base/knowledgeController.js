const { join, resolve } = require("path");
const fs = require("fs");
const createAsyncError = require("../../middleware/createAsyncError");
const Knowledge = require("../../models/knowledge_base/knowledgeModel");
const Category = require("../../models/master/categoryModel");
const Group = require("../../models/master/groupModel");
const ErrorHandler = require("../../utils/errorHandler");

// Create New Knowledge Base
exports.createKnowledgeBase = createAsyncError(async (req, res, next) => {
  const file = req.file;
  const fileName = file.originalname.split(" ").join("_");
  const rootPath = join(resolve(__dirname, "..", "..", "uploads"));
  const filePath = join(rootPath, fileName);

  const category = await Category.findById(req.body.category_id);

  if (!category) {
    return next(new ErrorHandler("Category not found", 404));
  }

  const group = await Group.findById(req.body.group_id);

  if (!group) {
    return next(new ErrorHandler("Group not found", 404));
  }

  if (!fs.existsSync(rootPath)) fs.mkdirSync(rootPath);

  fs.writeFile(filePath, file.buffer, (err) => {
    if (err) return next(new ErrorHandler("Failed to save file", 500));
  });

  const data = {
    ...req.body,
    filename: req.file.originalname,
    category_name: category.category_name,
    group_name: group.group_name,
    files: `${process.env.GLOBAL_URL}/api/v1/knowledge_base/file/${fileName}`,
  };

  const knowledge = await Knowledge.create(data);

  res.status(201).json({
    success: true,
    message: "Knowledge base added successfully",
    knowledge,
  });
});

// Get All Knowledge Base
exports.getAllKnowledgeBase = createAsyncError(async (req, res, next) => {
  const knowledges = await Knowledge.find();
  const knowledgeCount = await Knowledge.countDocuments();

  res.status(200).json({
    start: 0,
    group_id: "",
    category_id: "",
    success: true,
    message: "Knowledge Base Lists",
    data: {
      kb_list: knowledges,
      mainlist_count: [
        {
          count_res: knowledgeCount,
        },
      ],
    },
  });
});

// Get Single Knowledge Base Detail
exports.getSingleKnowledgeBase = createAsyncError(async (req, res, next) => {
  const knowledge = await Knowledge.findById(req.params.id);

  if (!knowledge) {
    return next(new ErrorHandler("Knowledge Base not found", 404));
  }

  res.status(200).json({
    success: true,
    message: "Knowledge base details",
    data: { result: knowledge },
  });
});

// Update Knowledge Base
exports.updateKnowledgeBase = createAsyncError(async (req, res, next) => {
  let knowledge = await Knowledge.findById(req.params.id);

  if (!knowledge) {
    return next(new ErrorHandler("Knowledge Base not found", 404));
  }

  if (req.file) {
    const file = req.file;
    const fileName = file.originalname.split(" ").join("_");
    const rootPath = join(resolve(__dirname, "..", "..", "uploads"));
    const filePath = join(rootPath, fileName);

    if (fs.existsSync(join(rootPath, knowledge.filename)))
      fs.unlinkSync(join(rootPath, knowledge.filename));

    fs.writeFile(filePath, file.buffer, (err) => {
      if (err) return next(new ErrorHandler("Failed to save file", 500));
    });

    req.body.filename = fileName;
    req.body.files = `${process.env.GLOBAL_URL}/api/v1/knowledge_base/file/${fileName}`;
  }

  if (req.body.group_id) {
    const group = await Group.findById(req.body.group_id);

    if (!group) return next(new ErrorHandler("Group not found", 404));

    if (req.body.category_id) {
      if (group.category_id.toString() !== req.body.category_id)
        return next(new ErrorHandler("Group doesn't match with category", 400));

      const category = await Category.findById(req.body.category_id);
      if (!category) return next(new ErrorHandler("Category not found", 404));

      req.body.category_name = category.category_name;
    } else if (group.category_id !== knowledge.category_id) {
      return next(new ErrorHandler("Group doesn't match with category", 400));
    }

    req.body.group_name = group.group_name;
  }

  knowledge = await Knowledge.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });

  res.status(200).json({
    success: true,
    message: "Knowledge base updated successfully",
    data: { result: knowledge },
  });
});

// Delete Knowledge Base
exports.deleteKnowledgeBase = createAsyncError(async (req, res, next) => {
  const knowledge = await Knowledge.findById(req.params.id);

  if (!knowledge) {
    return next(new ErrorHandler("Knowledge Base not found", 404));
  }

  const rootPath = join(resolve(__dirname, "..", "..", "uploads"));

  if (fs.existsSync(join(rootPath, knowledge.filename)))
    fs.unlinkSync(join(rootPath, knowledge.filename));

  await Knowledge.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "knowledge base deleted successfully",
  });
});

// get file for Knowledge Base
exports.getFile = (req, res, next) => {
  const file = join(
    resolve(__dirname, "..", "..", "uploads", req.params.filename)
  );

  if (!fs.existsSync(file)) {
    return next(new ErrorHandler("File not found", 404));
  }

  res.sendFile(file);
};
